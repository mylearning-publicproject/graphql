var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');
var fetch = require('node-fetch')

// Initialize a GraphQL schema
var schema = buildSchema(`
type Query{
    allNews: [News],
},
  type News {
    poster : PosterType,
    title  : String,
    content: String,
    likes  : PosterType,
    created: String,
  },
  type PosterType{
    phone : String,
    shortPhone : String,
    password : String,
  }
`);

// Root resolver
// var root = {
//   allNews: async () => {
//     const fetchData = await fetch('http://localhost:8008/news').then(async response => {
//         const data = await response.json()
//         return data;
//   });
//     console.log(fetchData[0].title, "data fetched" );

//     return fetchData;
//   }
// };

var root = {
    allNews: () => {
        const fetchData = fetch('http://localhost:8008/news').then(response => {
            const data = response.json();
            return data;
        })
        return  fetchData;
    }
}

// Create an express server and a GraphQL endpoint
var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,  // Must be provided
  rootValue: root,
  graphiql: true,  // Enable GraphiQL when server endpoint is accessed in browser
}));
app.listen(4000, () => console.log('Now browse to localhost:4000/graphql'));